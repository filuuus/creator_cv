<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\DB;

class DocumentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create()
    {
        return view('document.create');
    }

[
    'basic' => [
        'street' => 'required',
        'number_of_home' => 'required',
        'number_of_building' => 'required',
        'basic.post_code' => 'required',
        'basic.city' => 'required',
        'basic.phone_number' => 'required',
        'basic.email' => 'required',
        'basic.date_of_birth' => 'required',
        'basic.goal' => 'required',
        'basic.image' => 'required|image',
    ],

    'schools' => [
        [
            'start_year' => 'required',
            'end_year' => 'required',
            'name_of_school' => 'required',
            'city' => 'required',
        ],
        [
            'start_year' => 'required',
            'end_year' => 'required',
            'name_of_school' => 'required',
            'city' => 'required',
        ]
    ]
]

    protected function validator(array $data) {
        return Validator::make($data, [
            'basic.street' => 'required',
            'basic.number_of_home' => 'required',
            'basic.number_of_building' => 'required',
            'basic.post_code' => 'required',
            'basic.city' => 'required',
            'basic.phone_number' => 'required',
            'basic.email' => 'required',
            'basic.date_of_birth' => 'required',
            'basic.goal' => 'required',
            'basic.image' => 'required|image',

            'schools.*.start_year' => 'required',
            'schools.*.end_year' => 'required',
            'schools.*.name_of_school' => 'required',
            'schools.*.city' => 'required',
        ]);
    }

    public function index(Request $request)
    {
        $city = $request->city;
        $email = $request->email;
        $query = [];

        if ($city) {
            $query[] = ['city', '=', $city];
        }

        if ($email) {
            $query[] = ['email', '=', $email];
        }

        $users = User::with(['schools'])->where($query)->get();
        return UserResource::collection($users);
    }

    public function Store(Request $request) {

        $data = $this->validator($request->all())->validate();

        $user = auth()->user();
        Auth::

        if (!$user) {
            return response()->json([
                'title' => 'Error',
                'message' => 'Something went wrong'
            ], 404);
        }

        DB::beginTransaction();
        try {
            $imagePath = request('image')->store('photos', 'public');
            $user->basicinformation()->create([
                'street' => $data['basic']['street'],
                'number_of_home' => $data['basic']['number_of_home'],
                'number_of_building' => $data['basic']['number_of_building'],
                'post_code' => $data['post_code'],
                'city' => $data['city'],
                'phone_number' => $data['phone_number'],
                'email' => $data['email'],
                'date_of_birth' => $data['date_of_birth'],
                'goal' => $data['goal'],
                'image' => $imagePath
            ]);

            foreach($data['schools'] as $school) {

                auth()->user()->schools()->create([
                    'start_year' => $school['start_year'],
                    'end_year' => $school['end_year'],
                    'name_of_school' => $school['name_of_school'],
                    'city' => $school['city']
                ]);
            }

            foreach($data['experiences'] as $experience) {

                auth()->user()->experiences()->create([
                    'start_year' => $experience['start_year'],
                    'start_month' => $experience['start_month'],
                    'end_year' => $data['end_year'],
                    'end_month' => $data['end_month'],
                    'name_of_company' => $data['name_of_company'],
                    'position' => $data['position_' . $i],
                    'description' => $data['description_' . $i]
                ]);
            }

            auth()->user()-> certificates()->create([
                'certificate' => $data['certificate_' . $i],
                'city' => $data['city_' . $i],
                'start_month' => $data['start_month_' . $i],
                'start_year' => $data['start_year_' . $i],
                'end_month' => $data['end_month_' . $i],
                'end_year' => $data['end_year_' . $i]
            ]);

            auth()->user()-> projects()->create([
                'link' => $data['link_' . $i],
                'description' => $data['description_' . $i]
            ]);

            $user->has_cv = 1;
            user->save();

            DB::commit();
        } catch (Exception) {
            DB::rollBack();
            return response()->json([
                'title' => 'Error',
                'message' => 'Something went wrong'
            ], 404);
        }
        

        return response()->json([
            'title' => 'Success',
            'message' => 'Success',
            'user' => new UserResource(auth()->user()),
        ], 200);
    }
    public function schoolStoreJson()
    {
        $count_of_loop = (count(request()->all())-1)/4;
        for($i = 0; $i < $count_of_loop; $i++) {
            $data = request()->validate([
                'start_year_' . $i => 'required',
                'end_year_' . $i => 'required',
                'name_of_school_' . $i => 'required',
                'city_' . $i => 'required'
            ]);
        }

        return 'git';
    }

    public function experienceStoreJson()
    {
        $count_of_loop = (count(request()->all())-1)/7;
        for($i = 0; $i < $count_of_loop; $i++) {
            $data = request()->validate([
                'start_year_' . $i => 'required',
                'start_month_' . $i => 'required',
                'end_year_' . $i => 'required',
                'end_month_' . $i => 'required',
                'name_of_company_' . $i => 'required',
                'position_' . $i => 'required',
                'description_' . $i => 'required'
            ]);
        }

        return 'git';
    }

    public function certificateStoreJson()
    {
        $count_of_loop = (count(request()->all())-1)/6;
        for($i = 0; $i < $count_of_loop; $i++) {
            $data = request()->validate([
                'certificate_' . $i => 'required',
                'city_' . $i => 'required',
                'start_month_' . $i => 'required',
                'start_year_' . $i => 'required',
                'end_month_' . $i => 'required',
                'end_year_' . $i => 'required'
            ]);

        }

        return 'git';
    }

    public function projectStoreJson()
    {
        $count_of_loop = (count(request()->all())-1)/2;
        for($i = 0; $i < $count_of_loop; $i++) {
            $data = request()->validate([
                'link_' . $i => 'required',
                'description_' . $i => 'required'
            ]);

            
        } 

        return 'git';
    }

    public function save() {
        auth()->user()->has_cv = 1;
        auth()->user()->save();
        $id = auth()->user()->id;
        return redirect()->route('getPDF', ['user' => $id]);
    }
}


class SchoolResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name
        ];
    }

}

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'schools' => SchoolResource::collection($this->schools),
        ];
    }
}